## default

- black: #000
- white: #fff
- red: #ff0000
- green: #00ff00
- blue: #0000ff

## Bootstrap

- blue: #007bff;
- indigo: #6610f2;
- purple: #6f42c1;
- pink: #e83e8c;
- red: #dc3545;
- orange: #fd7e14;
- yellow: #ffc107;
- green: #28a745;
- teal: #20c997;
- cyan: #17a2b8;
- gray: #6c757d;
- gray-dark: #343a40;
- primary: #0970d2;
- secondary: #6c757d;
- success: #3ac47d;
- info: #30b1ff;
- warning: #f7b924;
- danger: #d92550;
- light: #eee;
- dark: #343a40;
- focus: #444054;
- alternate: #83588a;

## Red

- light-red: #f7aaaa

## Blue

- blue: #467fd7
- blue: #1d6bb5
- blue: #0b5c98
- blue: #076baf
- blue: #0a66b7
- blue: #1236ff
- light-blue: #E0F3FF

## Green

- green: #7cd175
- green: #63af43
- green: #00db00
- green: #6ab82d
- green: #02a17c
- green: rgba(84,194,66,1) #54c242
- light-green: #a6e3a1

## Yellow

- yellow: #ffd700
- yellow: #f8d486
- yellow: #f7f7aa
- yellow: #b5a16f

## Gray, Black

- gray: #555555
- gray: #999
- gray: #cccccc
- gray: rgb(141, 141, 141) #8d8d8d
- gray: #717171
- gray: #bfbdbd
- light-gray: #ece9e9
- light-gray: #c6c6c6
- light-gray: #b7b7b7
- light-gray: #efefef
- light-gray: #f1f1f1
- light-gray: #f7f7f7
- light-gray: #e6e6e5
- light-gray: #e9ecef
- light-gray: #f0fbff
- dark-gray: #495057
- dark-gray: #575f65
- dark-gray: #333
- black: #272822
